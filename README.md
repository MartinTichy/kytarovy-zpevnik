# Kytarovy-zpevnik

Kytarový zpěvník tvořený nadšenci D-L-M. Kdoliv může do tohoto projektu přispívat.

Pro tvorbu je nutné následující:
1. Nainstalovat verzi LaTeXu. Např. https://miktex.org/
2. Nainstalovat IDE Texmaker. Např. http://www.xm1math.net/texmaker/
	2.1 V Texmakeru v nastavení zapnout možnost "povolit pro výstupní soubory složku build"
